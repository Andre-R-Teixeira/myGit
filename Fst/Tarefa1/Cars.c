#include "Cars.h"

//function deletes one car that the user chooses 
CAR *deletCar(CAR *start, int serialNumber)
{
    CAR *after = NULL;
    CAR *startCopy;
    startCopy = start;

    //triyng to remove from a list of only 1 car
    if (start->next == NULL)
    {
        if (start->serial == serialNumber)
        {
            printf("Delet mode 1\n");
            printf("Freed Brand:%s Serial %d Speed %f\n", 
                    start->brand,
                    start->serial,
                    start->price);
            free(start);
            start = NULL;
            return start; 
        }
    }

    //removing the first car of a linked list
    if (startCopy->serial == serialNumber)
    {
        printf("delet mode 2\n");

        start = startCopy->next;
        
        printf("Free Brand:%s Serial %d Speed %f\n", 
                startCopy->brand,
                startCopy->serial,
                startCopy->price);
        
        free(startCopy);
        
        return start;
    }
    //removing a 'car' from the middle of the struc
    while (startCopy != NULL)
    {
        if(startCopy->serial == serialNumber)
        {
            printf("delet mode 3\n");
            after->next = startCopy->next;
            printf("Free Brand:%s Serial %d Speed %f\n", 
                startCopy->brand,
                startCopy->serial,
                startCopy->price);
            free(startCopy);
            return start;
        }
        after = startCopy;
        startCopy = startCopy->next;
    }
    
    printf("The car thar you chose does not exist \n");
    return start;
}

//function that makes possible to edit car 
void editCar(CAR *start, int serialNumber) 
{
    //  CAR *after = NULL;
    CAR *startCopy = NULL;
    startCopy = start;
    char input[64];

    while (startCopy != NULL)
    {
        if (startCopy->serial == serialNumber)
        {
            printf("Enter brand, serial and price all in the same line  \n all in the same line  \n");
            fgets(input, 63, stdin);
            sscanf(input, "%s %d %f", startCopy->brand, &startCopy ->serial, &startCopy ->price);
            printf("Added %s, serial: %d, price: %f \n\n", startCopy ->brand, startCopy ->serial, startCopy ->price); 
            return;
        }
        startCopy = startCopy ->next;
    }
}

//Selects the car based on the serial that was inputed
CAR *carSelector (CAR *start, int mode)
{
    char input [16];
    int value = 0;

    if(start == NULL)
    {
        return NULL;
    }   
    else
    {
        printf("Enter the selected serial number: \n");
        //fgets(input, 15, stdin
        while( fgets(input, 15, stdin) )
        {
            sscanf(input, "%d", &value);
            if (value < 0)
            {
                printf("Please insert a positive number \n");
            }
            else 
            {
                break;
            }
            printf("Enter the select serial number \n");
        }
    } 
    if (mode == 1) //delet car mode
    {
        start = deletCar(start, value);
        return start;
    }
    else if (mode == 2) //edit car mode
    {
        editCar(start, value);
        return start;
    }
    return NULL;
}

//This functions prints all the cars the the user added to the Garage
void PrintList(CAR *start)
{
    CAR *currentCar = NULL; 
    currentCar = start;
    int count = 0;

    while(currentCar != NULL)
    {
        count++;
        printf("---------------------------------------------------------------------------------------------------------------\n");
        printf("Car %d: \n\tBrand: %s \n\tSerial: %d \n\tPrice: %f\n", 
                count, 
                currentCar->brand,
                currentCar->serial,
                currentCar->price);
        printf("---------------------------------------------------------------------------------------------------------------\n");
        currentCar = currentCar->next;
    }
    printf("Total cars printed:%d \n", count);
}   

//Function adds a car to the garage
CAR *AddCar(CAR *previous)
{
    printf("Enter brand, serial and price all in the same line  \n\n");
    char input[64];
    fgets(input, 63, stdin);

    CAR *newCar = (CAR*)malloc(sizeof(CAR));
    newCar-> next = NULL;
    
    sscanf(input, "%s %d %f", newCar->brand, &newCar ->serial, &newCar ->price);
    printf("Added %s, serial: %d, price: %f \n\n", newCar ->brand, newCar ->serial, newCar ->price); 

    while (confirmInput (previous, newCar->serial) == 0)
    {
        //cleaning the string 
        input[0] = '\0';

        //asking for the new input
        printf("That serial number is already occupied \n");
        printf("Enter brand, serial and price all in the same line  \n\n");
        fgets(input, 63, stdin);
        sscanf(input, "%s %d %f", newCar->brand, &newCar ->serial, &newCar ->price);
        printf("Added %s, serial: %d, price: %f \n\n", newCar ->brand, newCar ->serial, newCar ->price); 
    }

    if (previous != NULL)
    {
        previous->next = newCar;
    }

    return newCar;
}

int confirmInput(CAR *start, int serialNumber)
{
    CAR *currentCar = NULL;
    currentCar = start;

    while (currentCar != NULL)
    {
        if (currentCar->serial == serialNumber)
        {
            return 0; //says that the car cannot have that same serial number since it is ocupied
        }
        currentCar = currentCar->next;
    }
    return 1;
}

//This functions set free all the memory that was dinamicly allocated
void Cleanup (CAR *start)
{
    CAR *freeMe = start;
    CAR *holdMe = NULL;

    while (freeMe != NULL)
    {
        holdMe = freeMe ->next;
        printf("Free brand:%s serial %d\n", 
                freeMe->brand,
                freeMe->serial);

        free(freeMe);
        freeMe = holdMe;
    }
}

int main()
{
    char command[16];
    char input[16];

    CAR *start = NULL;
    CAR *newest = NULL;

    printf("\nCommands to use: \n\
            add - Add another car to the garagen \n\
            print - Print the list of car in the garage \n\
            delet - Deletes a car from the garage \n\
            quit - Leaves the program \n\
            edit - edit one of the selected car\n");
    
    while(fgets(input, 15, stdin))
    {
        //receives user input on the command that will be excuted
        sscanf(input, "%s", command);

        //selection of the command based in the user input
        
        if (strncmp(command, "print", 5) == 0)
        {
            PrintList(start);
        }
        else if (strncmp (command, "delet", 5) == 0)
        {
            printf("Entering Delet...\n");
            start = carSelector(start, 1);
        }
        else if ( strncmp(command, "edit", 4) == 0)
        {
            printf("Entering edit... \n");
            start = carSelector(start, 2);
        }
        else if (strncmp(command, "quit", 4) == 0)
        {
            printf("Quitting...\n");
            break;
        }
        else if (strncmp(command, "add", 3) == 0)
        {
            if(start == NULL) 
            {
                start = AddCar(NULL);
                newest = start;
            }  
            else 
            {
                newest = AddCar(newest);
            }
        }
        else 
        {
            printf("Please input a defined command \n");
        }
    } 

    Cleanup(start);

    return 0;
}
