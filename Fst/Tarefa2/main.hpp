#ifndef MAIN
#define MAIN

#include "Classes.hpp"
#include "cars.hpp"

#include <iostream>
#include <string>
#include <string.h>

using namespace std;

void functionCaller(char *Input, CarDealer *Dealership, Car *CarHead, Motorcycle *MotoHead);

void printStartMessage();
int chooseType();

#endif