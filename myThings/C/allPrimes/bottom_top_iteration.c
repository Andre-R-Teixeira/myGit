 
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int check_prime(int number);

int main()
{
    time_t tempo_de_inicio = time(NULL);

    int prime = 0;
    int number = 1000000; //1 000 000;
    FILE* pToFile;
    pToFile = fopen("primeNumbers.txt", "w");

    for(int i = 1; i <= number; i++)
    {
        if(check_prime(i) == 1)
        {
            prime += 1;
            fprintf(pToFile, "%d \n", i);

        }
    }
    printf("%d \n", prime);
    time_t fim_de_jogo = time(NULL);

    printf("%u segundos\n", (unsigned)(fim_de_jogo - tempo_de_inicio));

}

int check_prime(int number)
{
    if (number == 2)
        return 1;
    for(int i = 2; i <= number - 1; i++)
    {
        if (number % i == 0)
            return 0;
    }
    return 1;
}
